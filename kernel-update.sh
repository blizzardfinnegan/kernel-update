#!/usr/bin/env sh

# Root check
if [[ "$EUID" -ne 0 ]]; then
   echo "Run as root."
   exit
fi


#Count kernel versions
declare -i counter
declare -a kernelversions
declare architecture=$(uname -m)
for i in /usr/src/linux-*; do
   kver=$(basename $i)
   #Grab the kernel version, but drop the "linux-" header
   kernelversions+=("${kver#*-}")
   ((counter++))
done

#Auto-determine number of processes to use
PROC=$(( $(nproc) + 1 ))
echo "===BUILDING WITH $PROC PROCESSES==="

#Ask if you want to build with defaults
read -p "Would you like to build with previous defaults? [Y/n]" defaultkernel
defaultkernel=$( echo "$defaultkernel" | tr '[:upper:]' '[:lower:]' )

#Notify if not building with defaults
[[ "$defaultkernel" = "N" || "$defaultkernel" = "n" ]] && \
echo "You will be taken to makeconfig on each kernel build. Please set values accordingly."

#initramfs request and (optional) check to see installed tools
read -p "Would you like to build an initramfs using dracut? [y/N]" initramfs
initramfs=$( echo "$initramfs" | tr '[:upper:]' '[:lower:]' )

#Loop over all available kernels
for (( i=1; i<=counter; i++ )); do
   echo "===BUILDING KERNEL ${kernelversions[$((i-1))]}==="
   echo "-------------------------"
   eselect kernel set $i
   #Remeber where the user is
   pushd /usr/src/linux >> /dev/null

   if [[ "$defaultkernel" = "n" ]]; then
      echo "===BUILDING KERNEL==="
      make menuconfig
   else
      echo "===BUILDING KERNEL WITH PREVIOUS CONFIG==="
      make olddefconfig
   fi

   echo "===MAKING KERNEL==="
   make -j$proc

   echo "===INSTALLING MODULES==="
   make modules_install

   echo "===INSTALLING KERNEL==="
   make install

   if [[ "$initramfs" = "y" ]]; then
      echo "===GENERATING INITRAMFS==="
      dracut --kver=${kernelversions[$((i-1))]}$(architecture);
   fi
   
   echo "===MAKING GRUB CONFIG==="
   grub-mkconfig -o /boot/grub/grub.cfg
   
   echo "===EXITED WITH CODE $?==="
   popd >> /dev/null

   echo "-------------------------"
done
